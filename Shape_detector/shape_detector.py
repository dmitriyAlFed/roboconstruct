# USAGE
# python detect_shapes.py --image shapes_and_colors.png

# import the necessary packages
import cv2
import numpy as np
""" не смотри
class SShapeDetector:
	def __init__(self):
		pass

	def detect(self, c, frame):
		shape = "unidentified"
		peri = cv2.arcLength(c, True)
		approx = cv2.approxPolyDP(c, 0.01 * peri, True)

		if len(approx) == 3:
			shape = "triangle"
			cv2.drawContours(frame, [approx], -1, (0, 255,255), 2)

		elif len(approx) == 4:

			(x, y, w, h) = cv2.boundingRect(approx)
			ar = w / float(h)

			shape = "square" if ar >= 0.95 and ar <= 1.05 else "rectangle"
			cv2.drawContours(frame, [approx], -1,(0, 255,255), 2)

		elif len(approx) == 5:
			shape = "pentagon"
			cv2.drawContours(frame, [approx], -1, (0, 255,255), 2)
		elif len(approx) == 6:
			shape = "hexagon"
			cv2.drawContours(frame, [approx], -1, (0, 255, 255), 2)
		elif len(approx) == 7:
			shape = "heptagon"
			cv2.drawContours(frame, [approx], -1, (0, 255, 255), 2)
		elif len(approx) == 8:
			shape = "octagon"
			cv2.drawContours(frame, [approx], -1, (0, 255, 255), 2)

		else:
			shape = "_"

		return shape
"""
class ShapeDetector:
	def __init__(self, shape, draw=False):
		self.shape = shape #shape.lower() if shape is str else shape
		self.draw = draw
		self.founded = []
		self.CANNY_LOW = 0
		self.CANNY_HIGH = 100
		self.COLOR_FRAME = (0, 255, 255)
		self.COLOR_SHAPE = (0, 0, 255)


	# cv2.drawContours(frame, approx, -1, self.COLOR_SHAPE, 2)
	def findTriangle(self, contour):
		peri = cv2.arcLength(contour, True)
		approx = cv2.approxPolyDP(contour, 0.1* peri, True)
		if len(approx) == 3:
			# shape = "triangle"
			(x, y, w, h) = cv2.boundingRect(approx)
			box = [x, y, x + w, y + h]
			return box, approx
		else:
			return None, approx

	def findRectangle(self, contour):
		peri = cv2.arcLength(contour, True)
		approx = cv2.approxPolyDP(contour, 0.1* peri, True)
		print('app', len(approx))
		if len(approx) == 4:
			# shape = "triangle"
			(x, y, w, h) = cv2.boundingRect(approx)
			box = [x, y, x + w, y + h]
			return box, approx
		else:
			return None, approx

	def findPentagon(self, contour):
		peri = cv2.arcLength(contour, True)
		approx = cv2.approxPolyDP(contour, 0.05* peri, True)
		if len(approx) == 5:
			# shape = "triangle"
			(x, y, w, h) = cv2.boundingRect(approx)
			box = [x, y, x + w, y + h]
			return box, approx
		else:
			return None, approx

	def findHexagon(self, contour):
		peri = cv2.arcLength(contour, True)
		approx = cv2.approxPolyDP(contour, 0.05* peri, True)
		if len(approx) == 6:
			# shape = "triangle"
			(x, y, w, h) = cv2.boundingRect(approx)
			box = [x, y, x + w, y + h]
			return box, approx
		else:
			return None, approx

	def findHeptagon(self, contour):
		peri = cv2.arcLength(contour, True)
		approx = cv2.approxPolyDP(contour, 0.01* peri, True)
		if len(approx) == 7:
			# shape = "triangle"
			(x, y, w, h) = cv2.boundingRect(approx)
			box = [x, y, x + w, y + h]
			return box, approx
		else:
			return None, approx

	def findOctagon(self, contour):
		peri = cv2.arcLength(contour, True)
		approx = cv2.approxPolyDP(contour, 0.01* peri, True)
		if len(approx) == 8:
			# shape = "triangle"
			(x, y, w, h) = cv2.boundingRect(approx)
			box = [x, y, x + w, y + h]
			return box, approx
		else:
			return None, approx

	def findCircle(self, borders_image, frame):
		boxes = []
		print('in')
		circles = cv2.HoughCircles(borders_image, cv2.HOUGH_GRADIENT, 1, 100, param1=50, param2=150, minRadius=0, maxRadius=1000)
		# circles = np.uint16(np.around(circles))
		if circles is not None:
			for i in circles[0, :]:
				cv2.circle(frame, (i[0], i[1]), i[2], self.COLOR_SHAPE, 2)
				box = [i[0]-int(i[2]/2), i[1]-int(i[2]/2), i[0]+int(i[2]/2), i[1]+int(i[2]/2)]
				boxes.append(box)
			return boxes
		else:
			return None
	def findEllipse(self):
		pass


	def process(self, frame):
		gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		gray = cv2.GaussianBlur(gray, (13, 13), 0)
		canny = cv2.Canny(gray, self.CANNY_LOW, self.CANNY_HIGH)
		cv2.namedWindow('canny', 0)
		cv2.imshow('canny', canny)
		if self.shape == 'circle':
			# print('circle')
			self.founded = self.findCircle(gray, frame)
			if self.draw and self.founded is not None:
				for box in self.founded:
					pass
					# cv2.cv2.rectangle(frame, (box[0], box[1]), (box[2], box[3]), self.COLOR_FRAME, 2)
			return self.founded, frame
		# dilate = cv2.dilate(canny, cv2.getStructuringElement(cv2.MORPH_CROSS, (3,3)), iterations = 1)
		_, contours, _ = cv2.findContours(canny, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
		o = 0
		for contour in contours:
			# o += 1
			# print(o)
			if self.shape == 'triangle' or 3:
				box, approx = self.findTriangle(contour)
				self.founded.append(box)
				if self.draw and box is not None:
					cv2.rectangle(frame, (box[0], box[1]), (box[2], box[3]), self.COLOR_FRAME, 2)
					cv2.drawContours(frame, approx, -1, self.COLOR_SHAPE, 2)
			elif self.shape == 'rectangle' or 4:
				print('112')
				box, approx = self.findRectangle(contour)
				self.founded.append(box)
				if self.draw and box is not None:
					cv2.rectangle(frame, (box[0], box[1]), (box[2], box[3]), self.COLOR_FRAME, 2)
					cv2.drawContours(frame, approx, -1, self.COLOR_SHAPE, 2)
			elif self.shape == 'pentagon' or 5:
				box, approx = self.findPentagon(contour)
				self.founded.append(box)
				if self.draw and box is not None:
					cv2.rectangle(frame, (box[0], box[1]), (box[2], box[3]), self.COLOR_FRAME, 2)
					cv2.drawContours(frame, approx, -1, self.COLOR_SHAPE, 2)
			elif self.shape == 'hexagon' or 6:
				box, approx = self.findHexagon(contour)
				self.founded.append(box)
				if self.draw and box is not None:
					cv2.rectangle(frame, (box[0], box[1]), (box[2], box[3]), self.COLOR_FRAME, 2)
					cv2.drawContours(frame, approx, -1, self.COLOR_SHAPE, 2)
			elif self.shape == 'heptagon' or 7:
				box, approx = self.findHeptagon(contour)
				self.founded.append(box)
				if self.draw and box is not None:
					cv2.rectangle(frame, (box[0], box[1]), (box[2], box[3]), self.COLOR_FRAME, 2)
					cv2.drawContours(frame, [approx], -1, self.COLOR_SHAPE, 2)
			elif self.shape == 'octagon' or 8:
				box, approx = self.findOctagon(contour)
				self.founded.append(box)
				if self.draw and box is not None:
					cv2.rectangle(frame, (box[0], box[1]), (box[2], box[3]),self.COLOR_FRAME, 2)
					cv2.drawContours(frame, approx, -1, self.COLOR_SHAPE, 2)
			cv2.drawContours(frame, contour, -1, (0, 255, 0), 2)
		return self.founded, frame


def nothing(x):
	pass
def main():
	# cap = cv2.VideoCapture(0)
	# sd = SShapeDetector()
	# cv2.namedWindow('Choose', 0)
	# cv2.namedWindow('drawing', 0)
	# cv2.createTrackbar("Low Canny", "Choose", 0, 255, nothing)
	# cv2.createTrackbar("High Canny ", "Choose", 0, 255, nothing)
	# cv2.createTrackbar("trigger", "drawing", 0, 1, nothing)
	# # frame0 = cv2.imread('test.jpg')
	# # frame = cv2.resize(frame0, (640, 480))
	# while True:
	# 	_, frame = cap.read()
	#
	# 	# frame = cv2.resize(frame0, (640, 480))
	#
	# 	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	# 	gray = cv2.GaussianBlur(gray, (7, 7), 0)
	# 	# mask = cv2.inRange(gray, 100, 255)
	# 	low = cv2.getTrackbarPos("Low Canny", "Choose")
	# 	high = cv2.getTrackbarPos("High Canny ", "Choose")
	# 	draw = cv2.getTrackbarPos('trigger', 'drawing')
	# 	canny = cv2.Canny(gray, low, high)
	#
	#
	# 	dilate = cv2.dilate(canny, cv2.getStructuringElement(cv2.MORPH_CROSS, (3,3)), iterations = 1)
	# 	dilate = cv2.erode(canny, cv2.getStructuringElement(cv2.MORPH_CROSS, (1, 1)), iterations=1)
	# 	contours, _ = cv2.findContours(dilate, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
	# 	print(len(contours))
	# 	if draw:
	# 		circles = cv2.HoughCircles(canny, cv2.HOUGH_GRADIENT, 1, 100, param1=50, param2=30, minRadius=0, maxRadius=0)
	# 		# circles = np.uint16(np.around(circles))
	# 		if circles is not None:
	# 			for i in circles[0, :]:
	# 				cv2.circle(frame, (i[0], i[1]), i[2], (0, 255, 0), 2)
	# 				cv2.circle(frame, (i[0], i[1]), 2, (0, 0, 255), 3)
	# 		for contour in contours:
	# 			# M = cv2.moments(c, 1)
	# 			# cX = int((M["m10"] / M["m00"]))
	# 			# cY = int((M["m01"] / M["m00"]))
	# 			shape = sd.detect(contour,frame)
	# 			# c = c.astype("float")
	# 			# c *= ratio
	# 			# c = c.astype("int")
	# 			if shape:
	# 				x, y, w, h = cv2.boundingRect(contour)
	# 				cv2.drawContours(frame, [contour], -1, (0, 255, 0), 2)
	# 				cv2.putText(frame, shape, (x, y), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 255), 2)
	#
	# 	cv2.namedWindow('Canny', 0)
	# 	cv2.imshow('Canny', dilate)
	# 	cv2.namedWindow('Frame', 0)
	# 	cv2.imshow('Frame', frame)
	# 	key = cv2.waitKey(10)
	# 	if key == 27:
	# 		break
	# cv2.destroyAllWindows()
	# cap.release()

	cap = cv2.VideoCapture(0)
	detector = ShapeDetector('circle', True)
	while True:
		_, frame = cap.read()

		founded, frame = detector.process(frame)
		cv2.namedWindow('frame', 0)
		cv2.imshow('frame', frame)
		# print(founded)

		key = cv2.waitKey(1)
		if key == 27:
			break
	cv2.destroyAllWindows()
	cap.release()

if __name__== '__main__':
	main()
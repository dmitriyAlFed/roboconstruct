#ifndef COLOR_DETECTOR_H
#define COLOR_DETECTOR_H

#include "omv/color.h"
#include "omv/types.h"

namespace omega {

class ColorDetector {
 public:
  ColorDetector();

  Frame Threshold(Frame&,                      // BGR matrix
                  const std::array<int, 3>&);  // Blur kernels (Gaussian and Median Blur)

  Frame Threshold(Frame&,                      // BGR matrix
                  const Color&,                // OmV Color
                  const std::array<int, 3>&);  // Blur kernels (Gaussian and Median Blur)

  Frame Threshold(Frame&,                      // BGR matrix
                  const cv::Vec3b&,            // Lower HSV border
                  const cv::Vec3b&,            // Upper HSV border
                  const std::array<int, 3>&);  // Blur kernels (Gaussian and Median Blur)
  void Erode(Frame&);

 private:
};

}  // namespace omega

#endif  // COLOR_DETECTOR_H

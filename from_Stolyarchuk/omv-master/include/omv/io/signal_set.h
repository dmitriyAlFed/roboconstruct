#ifndef SIGNAL_SET_H
#define SIGNAL_SET_H

#include <sys/signalfd.h>
#include <unistd.h>
#include <csignal>
#include <tuple>
#include <vector>

#include "handler.h"
#include "io.h"

class SignalSet {
 public:
  template <class... Args>
  SignalSet(Io& io, Args&&... args) : io_(io), sig_ints_{std::forward<Args>(args)...} {
    sigfillset(&blocked_mask_);
    sigemptyset(&allowed_mask_);

    for (int sig_int : sig_ints_)
      sigaddset(&allowed_mask_, sig_int);

    sigprocmask(SIG_SETMASK, &blocked_mask_, nullptr);

    fd_ = signalfd(-1, &allowed_mask_, 0);

    if (fd_ == -1)
      throw std::runtime_error("signal_fd_");
  }

  ~SignalSet() {
    close(fd_);
  }

  void AsyncWait(SignalsHandler::Sig&&);

 private:
  Io& io_;
  int fd_;
  sigset_t allowed_mask_;
  sigset_t blocked_mask_;

  std::vector<int> sig_ints_;
};

#endif  // SIGNAL_SET_H

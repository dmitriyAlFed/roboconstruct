#ifndef TASK_H
#define TASK_H

#include <future>

class Task {
 public:
  using Ptr = std::unique_ptr<Task>;
  using Future = std::future<void>;

  Task(Future&& future);

  void Run();

 private:
  std::future<void> future_;
};

#endif  // TASK_H

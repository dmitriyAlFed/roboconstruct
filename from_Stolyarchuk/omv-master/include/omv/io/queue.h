#ifndef QUEUE_H
#define QUEUE_H

#include <atomic>
#include <condition_variable>
#include <functional>
#include <future>
#include <mutex>
#include <queue>
#include <thread>

#include "task.h"

class Queue {
 public:
  Queue();
  ~Queue();

  void Start();
  void Stop();
  void Add(Task::Future&&);

 private:
  std::mutex lock_;
  std::vector<std::thread> threads_;
  std::queue<Task::Ptr> queue_;
  std::condition_variable cv_;
  bool stopped_;

  void Wait();
};

#endif  // QUEUE_H

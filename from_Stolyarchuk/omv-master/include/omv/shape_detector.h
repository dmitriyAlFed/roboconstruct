#ifndef SHAPE_DETECTOR_H
#define SHAPE_DETECTOR_H

#include <set>
#include <vector>

#include "omv/types.h"

namespace omega {

class ShapeDetector {
 public:
  ShapeDetector();

  void FilterBySize(Contours&, const std::set<size_t>&, size_t, size_t);
  void FilterByArea(Frame&, Result&, const std::vector<Coords>&);

  Contours FindContours(const Frame&);
  Contours ApproximateContours(const Contours&);
  Contour MakeSearchContour(const Frame&, const std::vector<Coords>&);
};

}  // namespace omega

#endif  // SHAPE_DETECTOR_H

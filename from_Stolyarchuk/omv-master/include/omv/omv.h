#ifndef OMV_H
#define OMV_H

#include <memory>
#include <set>
#include <vector>

#include <opencv2/opencv.hpp>

#include "omv/camera.h"
#include "omv/color.h"
#include "omv/color_detector.h"
#include "omv/shape_detector.h"
#include "omv/types.h"

namespace omega {

class OmV {
 public:
  OmV();

  Result FindShapesByColor(Frame&,                                 // OmV Frame
                           const Color&,                           // Color in BGR
                           const std::vector<Coords>& = {},        // Search Mask
                           const std::array<int, 3>& = {5, 5, 3},  // Blur kernels (Gaussian and Median Blur)
                           const std::set<size_t>& = {},           // Elements to return
                           size_t = 0,                             // Min size
                           size_t = MaxSize::max());               // Max size

  Result FindShapesByColor(Frame&,                                 // OmV Frame
                           const std::array<uchar, 3>&,            // Lower HSV color border
                           const std::array<uchar, 3>&,            // Upper HSV color border
                           const std::vector<Coords>& = {},        // Search Mask
                           const std::array<int, 3>& = {5, 5, 3},  // Blur kernels (Gaussian and Median Blur)
                           const std::set<size_t>& = {},           // Elements to return
                           size_t = 0,                             // Min size
                           size_t = MaxSize::max());               // Max size

  Result FindRectangles(Frame&,                                 // OmV Frame
                        const std::vector<Coords>& = {},        // Search Mask
                        const std::array<int, 3>& = {5, 5, 3},  // Blur kernels (Gaussian and Median Blur)
                        const std::set<size_t>& = {},           // Elements to return
                        size_t = 0,                             // Min size
                        size_t = MaxSize::max());               // Threshold Algorithm

  Result FindTriangles(Frame&,                                 // OmV Frame
                       const std::vector<Coords>& = {},        // Search Mask
                       const std::array<int, 3>& = {5, 5, 3},  // Blur kernels (Gaussian and Median Blur)
                       const std::set<size_t>& = {},           // Elements to return
                       size_t = 0,                             // Min size
                       size_t = MaxSize::max());               // Max size

  Result FindLines(Frame&,                                 // OmV Frame
                   const std::vector<Coords>& = {},        // Search Mask
                   const std::array<int, 3>& = {5, 5, 3},  // Blur kernels (Gaussian and Median Blur)
                   const std::set<size_t>& = {},           // Elements to return
                   size_t = 0,                             // Min size
                   size_t = MaxSize::max());               // Max size

  Result FindCircles(Frame&,                                 // OmV Frame
                     const std::vector<Coords>& = {},        // Search Mask
                     const std::array<int, 3>& = {5, 5, 3},  // Blur kernels (Gaussian and Median Blur)
                     const std::set<size_t>& = {},           // Elements to return
                     size_t = 0,                             // Min size
                     size_t = MaxSize::max());               // Max size

  void ShowFrame(Frame&&, const std::string& window_name);
  void ShowFrame(const Frame&, const std::string& window_name);
  void RegisterCamera(Camera*);

 private:
  Camera* camera_;
  ColorDetector color_detector_;
  ShapeDetector shape_detector_;

  Result FindShapesByColorImpl(Frame&,                           // Original frame
                               Frame&,                           // Thresholded frame
                               const std::vector<Coords>& = {},  // Search Mask
                               const std::set<size_t>& = {},     // Elements to return
                               size_t = 0,                       // Min size
                               size_t = MaxSize::max());         // Max size
};

}  // namespace omega

#endif  // OMV_H

#ifndef COLOR_H
#define COLOR_H

#include <opencv2/opencv.hpp>

namespace omega {

class Color {
 public:
  Color(uchar b, uchar g, uchar r) : bgr_{b, g, r} {}

  cv::Vec3b ToHSV() const;
  cv::Vec3b BGR() const;

  static std::map<std::string, cv::Range> basic_colors;
  //  {
  //      {"orange", std::pair{0, 22}}, {"yellow", std::pair{22, 38}},   {"green", std::pair{38, 75}},
  //      {"blue", std::pair{75, 130}}, {"violet", std::pair{130, 160}}, {"red", std::pair{160, 179}}};
  //{
  //      {"orange", std::pair{0, 22}}, {"yellow", std::pair{22, 38}},   {"green", std::pair{38, 75}},
  //      {"blue", std::pair{75, 130}}, {"violet", std::pair{130, 160}}, {"red", std::pair{160, 179}}};

 private:
  cv::Vec3b bgr_;
};

}  // namespace omega

#endif  // COLOR_H

#ifndef CAMERA_H
#define CAMERA_H

#include <atomic>
#include <functional>
#include <thread>

#include <opencv2/opencv.hpp>

#include "types.h"

namespace omega {

class Camera {
 public:
  using CameraCallback = std::function<void(Frame&&)>;

  Camera(int);
  ~Camera();

  void RegisterCallback(const CameraCallback&);

  void Start(int frame_rate);
  void Stop();

  inline friend std::ostream& operator<<(std::ostream& os, const Camera& c) {
    return os << "FPS:" << c.capture_device_.get(cv::CAP_PROP_FPS) << "; "          //
              << "Res: " << c.capture_device_.get(cv::CAP_PROP_FRAME_WIDTH) << 'x'  //
              << c.capture_device_.get(cv::CAP_PROP_FRAME_HEIGHT);
  }

  std::pair<int, int> GetRes() const {
    return std::pair(capture_device_.get(cv::CAP_PROP_FRAME_WIDTH), capture_device_.get(cv::CAP_PROP_FRAME_HEIGHT));
  }

 private:
  cv::VideoCapture capture_device_;
  std::atomic<bool> capturing_;

  CameraCallback callback_;
  std::unique_ptr<std::thread> capture_thread_;
};

}  // namespace omega

#endif  // CAMERA_H

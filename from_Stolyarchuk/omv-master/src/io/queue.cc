#include "io/queue.h"

#include <iostream>
#include <stdexcept>

#include "common/logger.h"

Queue::Queue() : stopped_(false) {}

Queue::~Queue() {
  if (!stopped_)
    Stop();

  for (auto& thread : threads_) {
    if (thread.joinable())
      thread.join();
  }
}

void Queue::Add(Task::Future&& future) {
  {
    std::unique_lock<std::mutex> lock(lock_);

    queue_.emplace(std::make_unique<Task>(std::move(future)));
  }
  cv_.notify_one();
}

void Queue::Start() {
  unsigned cpu_count = std::thread::hardware_concurrency();

  if (cpu_count <= 1)
    cpu_count = 1;
  else
    cpu_count /= 2;

  for (size_t i = 0; i < cpu_count; i++) {
    threads_.emplace_back(std::thread(&Queue::Wait, this));
  }
  LOG(DEBUG) << "queue started";
}

void Queue::Stop() {
  stopped_ = true;
  cv_.notify_all();

  LOG(DEBUG) << "queue stopped";
}

void Queue::Wait() {
  std::unique_lock<std::mutex> lock(lock_);

  while (!stopped_) {
    cv_.wait(lock, [this] { return (!queue_.empty() || stopped_); });

    if (!queue_.empty()) {
      auto task = std::move(queue_.front());
      queue_.pop();

      lock.unlock();
      task->Run();
      lock.lock();
    }
  }
}

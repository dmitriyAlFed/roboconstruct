#include "omv/omv.h"

#include <algorithm>

#include <opencv2/core/fast_math.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>

#include "omv/logger.h"
#include "omv/utils.h"

int test(int i) {
  return i * i;
}

namespace omega {

OmV::OmV() : camera_{nullptr} {
  //  cv::Range r(0, 10);

  //  LOG(DEBUG) << "time: " << r.start << ' ' << r.end;

  //  double t1 = static_cast<double>(cv::getTickCount());

  //  cv::parallel_for_(cv::Range(0, 100000000), [&](const cv::Range& range) {
  //    for (int r = range.start; r < range.end; r++)
  //      test(r);
  //  });

  //  double t2 = static_cast<double>(cv::getTickCount());

  //  LOG(DEBUG) << "time: " << (t2 - t1) / cv::getTickFrequency();
}

Result OmV::FindShapesByColor(Frame& frame,                            //
                              const Color& color,                      //
                              const std::vector<Coords>& mask,         //
                              const std::array<int, 3>& blur_kernels,  //
                              const std::set<size_t>& elements,        //
                              size_t min_size, size_t max_size) {
  Frame frame_thresholded = color_detector_.Threshold(frame, color, blur_kernels);
  Result results = FindShapesByColorImpl(frame, frame_thresholded, mask, elements, min_size, max_size);

  return results;
}

Result OmV::FindShapesByColor(Frame& frame,                            //
                              const std::array<uchar, 3>& hsv_lower,   //
                              const std::array<uchar, 3>& hsv_upper,   //
                              const std::vector<Coords>& mask,         //
                              const std::array<int, 3>& blur_kernels,  //
                              const std::set<size_t>& elements,        //
                              size_t min_size, size_t max_size) {
  if (hsv_lower.empty() || hsv_upper.empty())
    throw std::invalid_argument("empty hsv color array");

  if (hsv_lower[0] > 179 || hsv_upper[0] > 179) {
    uchar max_h = std::max(hsv_lower[0], hsv_upper[0]);
    throw std::invalid_argument(cv::format("invalid H in HSV color array: %d", max_h));
  }

  cv::Vec3b hsv_lower_vec(&hsv_lower[0]);
  cv::Vec3b hsv_upper_vec(&hsv_upper[0]);

  Frame frame_thresholded = color_detector_.Threshold(frame, hsv_lower_vec, hsv_upper_vec, blur_kernels);
  return FindShapesByColorImpl(frame, frame_thresholded, mask, elements, min_size, max_size);
}

Result OmV::FindRectangles(Frame& frame,                            //
                           const std::vector<Coords>& mask,         //
                           const std::array<int, 3>& blur_kernels,  //
                           const std::set<size_t>& elements,        //
                           size_t min_size, size_t max_size) {
  //  Frame frame_masked;

  //  int x = static_cast<int>(mask[0].x - 5);
  //  int y = static_cast<int>(mask[0].y - 5);

  //  int width = static_cast<int>(mask[1].x - mask[0].x + 5);
  //  int height = static_cast<int>(mask[3].y - mask[0].y + 5);

  //  cv::Rect rect(x, y, width, height);

  //  frame_masked.m = cv::Mat(frame.m, rect);

  //  Frame frame_thresholded = color_detector_.ThresholdFrame(frame, blur_kernels);
  Result results;

  return results;
}

void OmV::ShowFrame(Frame&& frame, const std::string& window_name) {
  cv::namedWindow(window_name, 1);
  cv::imshow(window_name, frame.m);

  if (static_cast<char>(cv::waitKey(30)) >= 0 && camera_ != nullptr)
    camera_->Stop();
}

void OmV::ShowFrame(const Frame& frame, const std::string& window_name) {
  cv::namedWindow(window_name, 1);
  cv::imshow(window_name, frame.m);

  if (static_cast<char>(cv::waitKey(0)) >= 0 && camera_ != nullptr)
    camera_->Stop();
}

void OmV::RegisterCamera(Camera* camera) {
  camera_ = camera;
}

Result OmV::FindShapesByColorImpl(Frame& frame,
                                  Frame& frame_thresholded,          //
                                  const std::vector<Coords>& mask,   //
                                  const std::set<size_t>& elements,  //
                                  size_t min_size, size_t max_size) {
  color_detector_.Erode(frame_thresholded);

  Result results;
  Contours contours = shape_detector_.FindContours(frame_thresholded);

  shape_detector_.FilterBySize(contours, elements, min_size, max_size);

  Contours contours_approx = shape_detector_.ApproximateContours(contours);

  for (size_t i = 0; i < contours_approx.size(); ++i)
    if (contours_approx[i].size() >= 4) {
      cv::Rect r = cv::boundingRect(contours[i]);
      results.emplace_back(std::make_shared<Rect>(r));
    }

  for (auto& result : results)
    cv::rectangle(frame.m, result->As<Rect>()->ToRect(), cv::Scalar{0, 0, 255}, 2, 4);

  if (!mask.empty())
    shape_detector_.FilterByArea(frame, results, mask);

  return results;
}

}  // namespace omega

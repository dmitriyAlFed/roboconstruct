#include "omv/shape_detector.h"

#include <iterator>
#include <numeric>

#include "omv/utils.h"

namespace omega {

ShapeDetector::ShapeDetector() {}

void ShapeDetector::FilterBySize(Contours& contours, const std::set<size_t>& elements, size_t min_size,
                                 size_t max_size) {
  std::sort(std::begin(contours), std::end(contours),
            [](const Contour& a, const Contour& b) { return cv::contourArea(a) > cv::contourArea(b); });

  if (min_size != 0 || max_size != MaxSize::max()) {
    auto erase_it = std::remove_if(std::begin(contours), std::end(contours),  //
                                   [min_size, max_size](const Contour& c) {
                                     return (cv::contourArea(c) < min_size || cv::contourArea(c) > max_size);
                                   });
    contours.erase(erase_it, std::end(contours));
  }

  if (!elements.empty()) {
    std::vector<size_t> contours_indecies(contours.size());
    std::vector<size_t> diff;

    std::iota(contours_indecies.begin(), contours_indecies.end(), 0);

    std::set_intersection(std::begin(contours_indecies), std::end(contours_indecies),  //
                          std::begin(elements), std::end(elements),                    //
                          std::back_inserter(diff));

    contours.erase(contours.begin(), helpers::FilterIndices(contours, std::begin(diff), std::end(diff)));
  }
}

void ShapeDetector::FilterByArea(Frame& frame, Result& results, const std::vector<Coords>& mask) {
  Contour search_contour = MakeSearchContour(frame, mask);

  if (!results.empty()) {
    auto erase_it = std::remove_if(std::begin(results), std::end(results),  //
                                   [&search_contour](Shape::Ptr shape)      //
                                   { return !shape->PolygonTest(search_contour); });

    results.erase(erase_it, std::end(results));
  }

  for (auto& result : results)
    cv::rectangle(frame.m, result->As<Rect>()->ToRect(), cv::Scalar{255, 0, 0}, 2, 4);

  cv::Rect r3 = cv::boundingRect(search_contour);
  cv::rectangle(frame.m, r3, cv::Scalar{0, 128, 255}, 2, 4);
}

Contours ShapeDetector::FindContours(const Frame& frame) {
  Contours contours;
  cv::findContours(frame.m, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

  return contours;
}

Contours ShapeDetector::ApproximateContours(const Contours& contours) {
  Contours contours_approx;
  Contour approx;

  for (auto& contour : contours) {
    cv::approxPolyDP(contour, approx, cv::arcLength(contour, true) * 0.02, true);
    contours_approx.push_back(approx);
  }

  return contours_approx;
}

Contour ShapeDetector::MakeSearchContour(const Frame& frame, const std::vector<Coords>& mask) {
  cv::Mat src = cv::Mat::zeros(cv::Size(frame.m.cols, frame.m.rows), CV_8UC1);
  std::vector<cv::Point2f> verts;

  for (const auto& coords : mask)
    verts.push_back({coords.x, coords.y});

  for (size_t i = 0; i < mask.size(); i++)
    cv::line(src, verts[i], verts[(i + 1) % mask.size()], cv::Scalar(255), 1);

  std::vector<std::vector<cv::Point>> contours;

  cv::findContours(src, contours, cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE);
  return contours[0];
}

}  // namespace omega

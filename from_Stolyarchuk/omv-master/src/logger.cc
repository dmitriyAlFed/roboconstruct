#include "omv/logger.h"

int NullBuffer::overflow(int c) {
  return c;
}

NullStream::NullStream() : std::ostream(&m_sb) {}

NullStream::~NullStream() {}

CoutStream::CoutStream() : std::ostream(std::cout.rdbuf()) {}

CoutStream::~CoutStream() {}

#include "omv/streamer.h"

#include "omv/logger.h"

#include <fstream>

void Streamer::Listener() {
  // send http header
  std::string header;
  header += "HTTP/1.0 200 OK\r\n";
  header += "Cache-Control: no-cache\r\n";
  header += "Pragma: no-cache\r\n";
  header += "Connection: close\r\n";
  header += "Content-Type: multipart/x-mixed-replace; boundary=mjpegstream\r\n\r\n";
  const size_t header_size = header.size();
  char* header_data = static_cast<char*>(header.data());
  fd_set rread;
  SOCKET maxfd;
  this->open();
  pthread_mutex_unlock(&mutex_writer);
  while (!stopped_) {
    rread = master;
    struct timeval to = {0, timeout};
    maxfd = sock + 1;

    int sel = select(maxfd, &rread, nullptr, nullptr, &to);
    if (sel > 0) {
      for (int s = 0; s < maxfd; s++) {
        if (FD_ISSET(s, &rread) && s == sock) {
          int addrlen = sizeof(SOCKADDR);
          SOCKADDR_IN address = {0};
          SOCKET client = accept(sock, reinterpret_cast<SOCKADDR*>(&address), reinterpret_cast<socklen_t*>(&addrlen));
          if (client == SOCKET_ERROR) {
            LOG(ERROR) << "error : couldn't accept connection on sock " << sock << " !";
            return;
          }
          maxfd = (maxfd > client ? maxfd : client);
          //          pthread_mutex_lock(&mutex_cout);
          LOG(DEBUG) << "new client " << client;
          char headers[4096] = "\0";
          ssize_t readBytes = _read(client, headers);
          LOG(DEBUG) << headers;
          //          pthread_mutex_unlock(&mutex_cout);
          pthread_mutex_lock(&mutex_client);
          _write(client, header_data, header_size);
          clients.push_back(client);
          pthread_mutex_unlock(&mutex_client);
        }
      }
    }
    usleep(1000);
  }
}

void Streamer::Writer() {
  pthread_mutex_lock(&mutex_writer);
  pthread_mutex_unlock(&mutex_writer);
  const int milis2wait = 16666;
  while (this->isOpened()) {
    pthread_mutex_lock(&mutex_client);
    int num_connected_clients = static_cast<int>(clients.size());
    pthread_mutex_unlock(&mutex_client);
    if (!num_connected_clients) {
      usleep(milis2wait);
      continue;
    }
    pthread_t threads[NUM_CONNECTIONS];
    int count = 0;

    std::vector<uchar> outbuf;
    std::vector<int> params;
    params.push_back(cv::IMWRITE_JPEG_QUALITY);
    params.push_back(quality);
    pthread_mutex_lock(&mutex_writer);
    imencode(".jpg", last_frame_, outbuf, params);
    pthread_mutex_unlock(&mutex_writer);
    int outlen = static_cast<int>(outbuf.size());

    pthread_mutex_lock(&mutex_client);
    std::vector<int>::iterator begin = clients.begin();
    std::vector<int>::iterator end = clients.end();
    pthread_mutex_unlock(&mutex_client);
    std::vector<clientPayload*> payloads;
    for (std::vector<int>::iterator it = begin; it != end; ++it, ++count) {
      if (count > NUM_CONNECTIONS)
        break;
      struct clientPayload* cp = new clientPayload({static_cast<Streamer*>(this), {outbuf.data(), outlen, *it}});
      payloads.push_back(cp);
      pthread_create(&threads[count], nullptr, &Streamer::clientWrite_Helper, cp);
    }
    for (; count > 0; count--) {
      pthread_join(threads[count - 1], nullptr);
      delete payloads.at(static_cast<size_t>(count - 1));
    }
    usleep(milis2wait);
  }
}

void Streamer::ClientWrite(clientFrame& cf) {
  stringstream head;
  head << "--mjpegstream\r\nContent-Type: image/jpeg\r\nContent-Length: " << cf.outlen << "\r\n\r\n";
  string string_head = head.str();
  pthread_mutex_lock(&mutex_client);
  _write(cf.client, (char*)string_head.c_str(), string_head.size());
  int n = _write(cf.client, (char*)(cf.outbuf), cf.outlen);
  if (n < cf.outlen) {
    std::vector<int>::iterator it;
    it = find(clients.begin(), clients.end(), cf.client);
    if (it != clients.end()) {
      LOG(ERROR) << "kill client " << cf.client;
      clients.erase(std::remove(clients.begin(), clients.end(), cf.client));
      ::shutdown(cf.client, 2);
    }
  }
  pthread_mutex_unlock(&mutex_client);
  pthread_exit(nullptr);
}

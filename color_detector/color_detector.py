import  cv2
import numpy as np
class ColorDetector():
    def __init__(self, color, draw=False):
        self.color = color
        self.draw = draw
        self.RED = [(0, 10), (80, 255), (0, 255)]
        self.ORANGE = [(10, 20), (150, 255), (0, 255)]
        self.YELLOW = [(20, 40), (150, 255), (0, 255)]
        self.GREEN = [(40, 85), (40, 255), (0, 255)]
        self.BLUE = [(80, 125), (90, 255), (0, 255)]
        self.PURPLE = [(115, 175), (40, 255), (0, 255)]
        self.WHITE = [(0, 180), (0, 255), (240, 255)]
        self.BLACK = [(0, 10), (80, 255), (0, 255)]
        self.founded = []
        self.choose = True
        self.low = 0
        self.up = 0

    def nothing(self, x):
        pass

    def findRed(self):
        self.low = np.array([self.RED[0][0], self.RED[1][0], self.RED[2][0]])
        self.up = np.array([self.RED[0][1], self.RED[1][1], self.RED[2][1]])
        # return low, up

    def findOrange(self):
        self.low = np.array([self.ORANGE[0][0], self.ORANGE[1][0], self.ORANGE[2][0]])
        self.up = np.array([self.ORANGE[0][1], self.ORANGE[1][1], self.ORANGE[2][1]])
        # return low, up

    def findYellow(self):
        self.low = np.array([self.YELLOW[0][0], self.YELLOW[1][0], self.YELLOW[2][0]])
        self.up = np.array([self.YELLOW[0][1], self.YELLOW[1][1], self.YELLOW[2][1]])
        # return low, up

    def findGreen(self):
        self.low = np.array([self.GREEN[0][0], self.GREEN[1][0], self.GREEN[2][0]])
        self.up = np.array([self.GREEN[0][1], self.GREEN[1][1], self.GREEN[2][1]])
        # return low, up

    def findBlue(self):
        self.low = np.array([self.BLUE[0][0], self.BLUE[1][0], self.BLUE[2][0]])
        self.up = np.array([self.BLUE[0][1], self.BLUE[1][1], self.BLUE[2][1]])
        # return low, up

    def findPurple(self):
        self.low = np.array([self.PURPLE[0][0], self.PURPLE[1][0], self.PURPLE[2][0]])
        self.up = np.array([self.PURPLE[0][1], self.PURPLE[1][1], self.PURPLE[2][1]])
        # return low, up

    def findWhite(self):
        self.low = np.array([self.WHITE[0][0], self.WHITE[1][0], self.WHITE[2][0]])
        self.up = np.array([self.WHITE[0][1], self.WHITE[1][1], self.WHITE[2][1]])
        # return low, up

    def findBlack(self):
        self.low = np.array([self.BLACK[0][0], self.BLACK[1][0], self.BLACK[2][0]])
        self.up = np.array([self.BLACK[0][1], self.BLACK[1][1], self.BLACK[2][1]])
        # return low, up

    def findYourColor(self):

        if self.choose:
            cv2.namedWindow("Choose", 0)
            cv2.createTrackbar("Low - H", "Choose", 0, 179, self.nothing)
            cv2.createTrackbar("Low - S", "Choose", 0, 255, self.nothing)
            cv2.createTrackbar("Low - V", "Choose", 0, 255, self.nothing)
            cv2.createTrackbar("Up - H", "Choose", 179, 179, self.nothing)
            cv2.createTrackbar("Up - S", "Choose", 255, 255, self.nothing)
            cv2.createTrackbar("Up - V", "Choose", 255, 255, self.nothing)
        while self.choose:
            palette = cv2.imread('test.jpg')
            hsv = cv2.cvtColor(palette, cv2.COLOR_BGR2HSV)
            low_h = cv2.getTrackbarPos("Low - H", "Choose")
            low_s = cv2.getTrackbarPos("Low - S", "Choose")
            low_v = cv2.getTrackbarPos("Low - V", "Choose")
            up_h = cv2.getTrackbarPos("Up - H", "Choose")
            up_s = cv2.getTrackbarPos("Up - S", "Choose")
            up_v = cv2.getTrackbarPos("Up - V", "Choose")
            self.low = np.array([low_h, low_s, low_v])
            self.up = np.array([up_h, up_s, up_v])
            # print(low, up)
            mask = cv2.inRange(hsv, self.low, self.up)
            palette = cv2.bitwise_and(palette, palette, mask=mask)
            cv2.imshow("Choose", palette)
            # print(low, up)
            key = cv2.waitKey(1)
            if key == 121 or key == 27:
                self.choose = False
                cv2.destroyWindow("Choose")
                break
                # return low, up

        # cv2.destroyWindow("Choose")
        # print (low, up)
        # return low, up

    def process(self, frame):
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        if self.color.lower() == 'red':
            self.findRed()
            mask = cv2.inRange(hsv, self.low, self.up)
            # dilate = cv2.dilate(mask, cv2.getStructuringElement(cv2.MORPH_CROSS, (50,50)), iterations = 1)
            # erode = cv2.erode(dilate, cv2.getStructuringElement(cv2.MORPH_CROSS, (50, 50)), iterations=1)
            _, contours, _ = cv2.findContours(mask,cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
            for contour in contours:
                if cv2.contourArea(contour) > 500:
                    x, y, w, h = cv2.boundingRect(contour)
                    box = [x, y, x + w, y + h]
                    self.founded.append(box)
                    if self.draw:
                        frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 255, 255), 2)

                #
            result = cv2.bitwise_and(frame, frame, mask=mask)
            return self.founded, frame
        elif self.color.lower() == 'orange':
            self.findOrange()
            mask = cv2.inRange(hsv, self.low, self.up)
            # dilate = cv2.dilate(mask, cv2.getStructuringElement(cv2.MORPH_CROSS, (50,50)), iterations = 1)
            # erode = cv2.erode(dilate, cv2.getStructuringElement(cv2.MORPH_CROSS, (50, 50)), iterations=1)
            _, contours, _ = cv2.findContours(mask,cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
            for contour in contours:
                if cv2.contourArea(contour) > 500:
                    x, y, w, h = cv2.boundingRect(contour)
                    box = [x, y, x + w, y + h]
                    self.founded.append(box)
                    if self.draw:
                        frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 255, 255), 2)

                #
            result = cv2.bitwise_and(frame, frame, mask=mask)
            return self.founded, frame
        elif self.color.lower() == 'yellow':
            self.findYellow()
            mask = cv2.inRange(hsv, self.low, self.up)
            # dilate = cv2.dilate(mask, cv2.getStructuringElement(cv2.MORPH_CROSS, (50,50)), iterations = 1)
            # erode = cv2.erode(dilate, cv2.getStructuringElement(cv2.MORPH_CROSS, (50, 50)), iterations=1)
            _, contours, _ = cv2.findContours(mask,cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
            for contour in contours:
                if cv2.contourArea(contour) > 500:
                    x, y, w, h = cv2.boundingRect(contour)
                    box = [x, y, x + w, y + h]
                    self.founded.append(box)
                    if self.draw:
                        frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 255, 255), 2)

                #
            result = cv2.bitwise_and(frame, frame, mask=mask)
            return self.founded, frame
        elif self.color.lower() == 'green':
            self.findGreen()
            mask = cv2.inRange(hsv, self.low, self.up)
            # dilate = cv2.dilate(mask, cv2.getStructuringElement(cv2.MORPH_CROSS, (50,50)), iterations = 1)
            # erode = cv2.erode(dilate, cv2.getStructuringElement(cv2.MORPH_CROSS, (50, 50)), iterations=1)
            _, contours, _ = cv2.findContours(mask,cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
            for contour in contours:
                if cv2.contourArea(contour) > 500:
                    x, y, w, h = cv2.boundingRect(contour)
                    box = [x, y, x + w, y + h]
                    self.founded.append(box)
                    if self.draw:
                        frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 255, 255), 2)

                #
            result = cv2.bitwise_and(frame, frame, mask=mask)
            return self.founded, frame
        elif self.color.lower() == 'blue':
            self.findBlue()
            mask = cv2.inRange(hsv,self.low, self.up)
            # dilate = cv2.dilate(mask, cv2.getStructuringElement(cv2.MORPH_CROSS, (50,50)), iterations = 1)
            # erode = cv2.erode(dilate, cv2.getStructuringElement(cv2.MORPH_CROSS, (50, 50)), iterations=1)
            _, contours, _ = cv2.findContours(mask,cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
            for contour in contours:
                if cv2.contourArea(contour) > 500:
                    x, y, w, h = cv2.boundingRect(contour)
                    box = [x, y, x + w, y + h]
                    self.founded.append(box)
                    if self.draw:
                        frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 255, 255), 2)

                #
            result = cv2.bitwise_and(frame, frame, mask=mask)
            return self.founded, frame
        elif self.color.lower() == 'purple':
            self.findPurple()
            mask = cv2.inRange(hsv, self.low, self.up)
            # dilate = cv2.dilate(mask, cv2.getStructuringElement(cv2.MORPH_CROSS, (50,50)), iterations = 1)
            # erode = cv2.erode(dilate, cv2.getStructuringElement(cv2.MORPH_CROSS, (50, 50)), iterations=1)
            _, contours, _ = cv2.findContours(mask,cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
            for contour in contours:
                if cv2.contourArea(contour) > 500:
                    x, y, w, h = cv2.boundingRect(contour)
                    box = [x, y, x + w, y + h]
                    self.founded.append(box)
                    if self.draw:
                        frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 255, 255), 2)

                #
            result = cv2.bitwise_and(frame, frame, mask=mask)
            return self.founded, frame
        elif self.color.lower() == 'white':
            self.findWhite()
            mask = cv2.inRange(hsv, self.low, self.up)
            # dilate = cv2.dilate(mask, cv2.getStructuringElement(cv2.MORPH_CROSS, (50,50)), iterations = 1)
            # erode = cv2.erode(dilate, cv2.getStructuringElement(cv2.MORPH_CROSS, (50, 50)), iterations=1)
            _, contours, _ = cv2.findContours(mask,cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
            for contour in contours:
                if cv2.contourArea(contour) > 500:
                    x, y, w, h = cv2.boundingRect(contour)
                    box = [x, y, x + w, y + h]
                    self.founded.append(box)
                    if self.draw:
                        frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 255, 255), 2)

                #
            result = cv2.bitwise_and(frame, frame, mask=mask)
            return self.founded, frame
        elif self.color.lower() == 'black':
            self.findBlack()
            mask = cv2.inRange(hsv, self.low, self.up)
            # dilate = cv2.dilate(mask, cv2.getStructuringElement(cv2.MORPH_CROSS, (50,50)), iterations = 1)
            # erode = cv2.erode(dilate, cv2.getStructuringElement(cv2.MORPH_CROSS, (50, 50)), iterations=1)
            _, contours, _ = cv2.findContours(mask,cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
            for contour in contours:
                if cv2.contourArea(contour) > 500:
                    x, y, w, h = cv2.boundingRect(contour)
                    box = [x, y, x + w, y + h]
                    self.founded.append(box)
                    if self.draw:
                        frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 255, 255), 2)

                #
            result = cv2.bitwise_and(frame, frame, mask=mask)
            return self.founded, frame
        else:
            self.findYourColor()
            mask = cv2.inRange(hsv, self.low, self.up)
            # dilate = cv2.dilate(mask, cv2.getStructuringElement(cv2.MORPH_CROSS, (50,50)), iterations = 1)
            # erode = cv2.erode(dilate, cv2.getStructuringElement(cv2.MORPH_CROSS, (50, 50)), iterations=1)
            _, contours, _ = cv2.findContours(mask,cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
            # for contour in contours:
            #     if cv2.contourArea(contour) > 500:
            #         x, y, w, h = cv2.boundingRect(contour)
            #         box = [x, y, x + w, y + h]
            #         self.founded.append(box)
            #         if self.draw:
            #             frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 255, 255), 2)

                #
            result = cv2.bitwise_and(frame, frame, mask=mask)
            cv2.namedWindow('result', 0)
            cv2.imshow('result', result)
            return self.founded, frame



def main():
    cap = cv2.VideoCapture(0)

    detector = ColorDetector('blue', True)
    while True:
        # founded = []
        _, frame = cap.read()
        founded, frame = detector.process(frame)
        cv2.namedWindow('frame', 0)
        cv2.imshow('frame', frame)
        print(founded)

        key = cv2.waitKey(1)
        if key == 27:
            break
    cv2.destroyAllWindows()
    cap.release()

if __name__ == '__main__':
    main()
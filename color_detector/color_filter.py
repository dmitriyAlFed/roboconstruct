import cv2
import numpy as np

def nothing(x):
    pass

# cv2.namedWindow( "settings" ) # создаем окно настроек
# cv2.createTrackbar('h1', 'settings', 0, 179, nothing)
# cv2.createTrackbar('s1', 'settings', 0, 255, nothing)
# cv2.createTrackbar('v1', 'settings', 0, 255, nothing)
# cv2.createTrackbar('h2', 'settings', 180, 180, nothing)
# cv2.createTrackbar('s2', 'settings', 255, 255, nothing)
# cv2.createTrackbar('v2', 'settings', 255, 255, nothing)
# crange = [0,0,0, 0,0,0]
#
# cap = cv2.VideoCapture(0)
#
#
# color_yellow = (0, 255, 255)
#
# while True:
#     h1 = cv2.getTrackbarPos('h1', 'settings')
#     s1 = cv2.getTrackbarPos('s1', 'settings')
#     v1 = cv2.getTrackbarPos('v1', 'settings')
#     h2 = cv2.getTrackbarPos('h2', 'settings')
#     s2 = cv2.getTrackbarPos('s2', 'settings')
#     v2 = cv2.getTrackbarPos('v2', 'settings')
#
#     # формируем начальный и конечный цвет фильтра
#     h_min = np.array((h1, s1, v1), np.uint8)
#     h_max = np.array((h2, s2, v2), np.uint8)
#     print(h_min[0],h_min[1],h_min[2])
#     print(h_max[0],h_max[1],h_max[2])
#
#     flag, img = cap.read()
#     hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
#     thresh = cv2.inRange(hsv, h_min, h_max)
#
#     moments = cv2.moments(thresh, 1)
#     dM01_blue = moments['m01']
#     dM10_blue = moments['m10']
#     dArea_blue = moments['m00']
#     img = cv2.bitwise_and(img, img, mask=thresh)
#     if dArea_blue > 700:
#
#         x = int(dM10_blue / dArea_blue)
#         y = int(dM01_blue / dArea_blue)
#         cv2.circle(img, (x, y), 5, color_yellow, 2)
#         cv2.putText(img, "%d-%d" % (x, y), (x + 10, y - 10),
#                     cv2.FONT_HERSHEY_SIMPLEX, 1, color_yellow, 2)
#
#     cv2.imshow('hresh_blue', thresh)
#     cv2.imshow('hsv', hsv)
#     cv2.imshow('blue', img)
#
#
#
#     ch = cv2.waitKey(5)
#     if ch == 27:
#         break
#
# cap.release()
# cv2.destroyAllWindows()

# cap = cv2.VideoCapture(0)
cv2.namedWindow("hsv", 0)
cv2.createTrackbar("L - H", "hsv", 0, 179, nothing)
cv2.createTrackbar("L - S", "hsv", 0, 255, nothing)
cv2.createTrackbar("L - V", "hsv", 0, 255, nothing)
cv2.createTrackbar("U - H", "hsv", 179, 179, nothing)
cv2.createTrackbar("U - S", "hsv", 255, 255, nothing)
cv2.createTrackbar("U - V", "hsv", 255, 255, nothing)
founded = []
# detector = ColorDetector('another', True)
while True:
    # founded = []
    # _, frame = cap.read()
    # founded, frame = detector.process(frame)
    frame = cv2.imread('test.jpg')
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    l_h = cv2.getTrackbarPos("L - H", "hsv")
    l_s = cv2.getTrackbarPos("L - S", "hsv")
    l_v = cv2.getTrackbarPos("L - V", "hsv")
    u_h = cv2.getTrackbarPos("U - H", "hsv")
    u_s = cv2.getTrackbarPos("U - S", "hsv")
    u_v = cv2.getTrackbarPos("U - V", "hsv")
    low_blue = np.array([l_h, l_s, l_v])
    up_blue = np.array([u_h, u_s, u_v])
    mask = cv2.inRange(hsv, low_blue, up_blue)
    dil = cv2.dilate(mask, cv2.getStructuringElement(cv2.MORPH_RECT, (100, 30)), iterations=1)
    erode = cv2.erode(dil, cv2.getStructuringElement(cv2.MORPH_RECT, (100, 30)), iterations=1)
    contours, _ = cv2.findContours(erode, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    for contour in contours:
        x, y, w, h = cv2.boundingRect(contour)
        box = [x, y, x + w, y + h]
        founded.append(box)
        frame = cv2.rectangle(frame,(x,y),(x+w,y+h),(255,255,255),2)

        #
    hsv = cv2.bitwise_and(frame, frame, mask=mask)
    cv2.namedWindow('frame', 0)
    cv2.namedWindow('hsv', 0)
    cv2.namedWindow('mask', 0)
    cv2.resizeWindow('frame', (640, 480))

    cv2.imshow('frame', frame)
    print(founded)
    cv2.imshow('hsv', hsv)
    cv2.imshow('mask', mask)
    key = cv2.waitKey(1)
    if key == 27:
        break
cv2.destroyAllWindows()
# cap.release()

